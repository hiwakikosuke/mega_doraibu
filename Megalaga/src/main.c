#include <genesis.h>
#include <resources.h>

#define MAX_ENEMIES 6
#define LEFT_EDGE 0
#define RIGHT_EDGE 320

typedef struct
{
    int x;
    int y;
    int w;
    int h;
    int velx;
    int vely;
    int health;
    Sprite* sprite;
    char name[6];
} Entity;

Entity player = {0, 0, 16, 16, 0, 0, 0, NULL, "PLAYER"};
Entity enemies[MAX_ENEMIES];

void killEntity(Entity* e)
{
    e->health = 0;
    SPR_setVisibility(e->sprite, HIDDEN);
}

void reviveEntity(Entity* e)
{
    e->health = 1;
    SPR_setVisibility(e->sprite,VISIBLE);
}

void positionEnemies()
{
    u16 i = 0;
    for(i=0; i<MAX_ENEMIES; i++)
    {
        Entity* e = &enemies[i];
        if(e->health > 0)
        {
            if((e->x + e->w) > RIGHT_EDGE)
            {
                e->velx = -1;
            }
            else if(e->x < LEFT_EDGE)
            {
                e->velx = 1;
            }
            e->x += e->velx;
            SPR_setPosition(e->sprite, e->x, e->y);
        }
    }
}

int main()
{
    SYS_disableInts();

    u16 enemiesLeft = 0;
    Entity* e = enemies;
    int i = 0;
    int thex = 0;
    int they = 0;
    int val = 1;
    int offset = 0;
    
    SPR_init();
    VDP_setPalette(PAL1, backgroud.palette->data);
    VDP_setPalette(PAL2, backgroud.palette->data);

    player.x = 152;
    player.y = 192;
    player.health = 1;
    player.sprite = SPR_addSprite(&ship, player.x, player.y, TILE_ATTR(PAL1, 0, FALSE, FALSE));
    
    for(i = 0; i < MAX_ENEMIES; i++)
    {
        e->x = i*32;
        e->y = 32;
        e->w = 16;
        e->h = 16;
        e->velx = 1;
        e->health = 1;
        e->sprite = SPR_addSprite(&ship, e->x, e->y, TILE_ATTR(PAL2, 0, TRUE, FALSE));
        sprintf(e->name, "En%d", i);

        enemiesLeft++;
        e++;
    }

    VDP_setPaletteColor(34, RGB24_TO_VDPCOLOR(0x0078f8));
    SPR_update();
    VDP_loadTileSet(backgroud.tileset, 1, DMA);
    VDP_setScrollingMode(HSCROLL_PLANE, VSCROLL_PLANE);
    SYS_enableInts();

    for(i=0; i<1280; i++)
    {
        thex = i % 40;
        they = i / 40;

        val = (random() % (10 - 1 + 1)) + 1;
        if(val > 3)
        {
            val = 1;
        }

        VDP_setTileMapXY(BG_B, TILE_ATTR_FULL(PAL1, 0, 0, 0, val), thex, they);
    }

    while(1)
    {
        VDP_setVerticalScroll(BG_B, offset -= 2);
        if(offset <= -256)
        {
            offset = 0;
        }

        positionEnemies();
        SPR_update();
        SYS_doVBlankProcess();
    }
    return (0);
}
