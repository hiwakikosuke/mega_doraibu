#include <genesis.h>
#include <resources.h>

typedef struct
{
    int number;//ステージの名前とか番号とか
    int tate;
    int yoko;
    int beam[];//大きさはyoko ビーム売ってる状態かどうか
    int enemies[];//大きさはyoko 敵(またはビームをあててはいけない味方)がいるか
    int field[][];//大きさはtate×yoko
    int health;//healthが0になるまでステージをリトライできる、とか？
} Stage;

/*
↓こういうの作りたい

pzprv3
hashikake
9
9
3 . . 4 . . 3 . .
. . 3 . 2 . . . 1
. . . 2 . . 4 . .
4 . 5 . . 2 . . .
. . . 3 . . 4 . 4
. . . . 2 . . 3 .
3 . . 4 . . 2 . 1
. . 2 . . 1 . . .
3 . . . 3 . . 2 .
0 0 0 0 0 0 -1 0
0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0
0 0 0 2 2 2 0 0
0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0

*/
