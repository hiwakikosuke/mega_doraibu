#ifndef _RES_RESOURCES_H_
#define _RES_RESOURCES_H_

extern const Image bg_image;
extern const Image test_image;
extern const Image backtest_image;
extern const Image bird_image;
extern const Image rkbg_image;

#endif // _RES_RESOURCES_H_
